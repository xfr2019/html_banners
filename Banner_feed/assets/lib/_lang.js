$(document).ready(function(){
  const userLang = navigator.language || navigator.userLanguage;
  console.log ("The language is: " + userLang); 
  switch(userLang) {
    case 'en': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-gb': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-us': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-ca': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-au': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-bz': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-au': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-ie': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-jm': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-nz': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-ph': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-za': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-tt': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'en-zw': 
    $.MultiLanguage('lib/language.json', 'en');
    break;
    case 'ru':
    $.MultiLanguage('lib/language.json', 'ru');
    break;
    case 'ru-mo':
    $.MultiLanguage('lib/language.json', 'ru');
    break;
    case 'ru-RU':
    $.MultiLanguage('lib/language.json', 'ru');
    break;
    case 'uk':
    $.MultiLanguage('lib/language.json', 'uk');
    break;
    default:
    $.MultiLanguage('lib/language.json', 'ru');
}

});