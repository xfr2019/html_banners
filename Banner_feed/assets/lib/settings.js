var wow = new WOW(
	{
		boxClass:     'wow',      
		animateClass: 'animated', 
		offset:       0,          
		mobile:       false,      
		live:         true,       
		callback:     function(box) {
		},
		scrollContainer: null,   
		resetAnimation: true,   
	}
	);
	wow.init();


		 $(document).ready(function(){
		 	const userLang = navigator.language || navigator.userLanguage;
		// 	console.log ("The language is: " + userLang);
		// 	const languages = ['en', 'ru', 'uk', 'uz', 'kz', 'kk', 'az', 'hy', 'el'];
		// 	const language = userLang.replace(/^(\w){2}[-_]?.*/, '$1');

		// 	if (languages.includes(language)) {
		// 		$.MultiLanguage('./assets/lib/language.json', language);
		// 	}
		// 	else {
		// 		$.MultiLanguage('./assets/lib/language.json', 'en');
		// 	}

			switch(userLang) {
				case 'en': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-gb': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-us': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-ca': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-au': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-bz': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-au': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-ie': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-jm': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-nz': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-ph': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-za': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-tt': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'en-zw': 
				$.MultiLanguage('./assets/lib/language.json', 'en');
				break;
				case 'ru':
				$.MultiLanguage('./assets/lib/language.json', 'ru');
				break;
				case 'ru-mo':
				$.MultiLanguage('./assets/lib/language.json', 'ru');
				break;
				case 'ru-RU':
				$.MultiLanguage('./assets/lib/language.json', 'ru');
				break;
				case 'uk':
				$.MultiLanguage('./assets/lib/language.json', 'uk');
				break;
				case 'uz':
				$.MultiLanguage('./assets/lib/language.json', 'uz');
				break;
				case 'uz-UZ':
				$.MultiLanguage('./assets/lib/language.json', 'uz');
				break;
				case 'kz':
				$.MultiLanguage('./assets/lib/language.json', 'ru');
				break;
				case 'kk':
				$.MultiLanguage('./assets/lib/language.json', 'kk');
				break;
				case 'kk-KZ':
				$.MultiLanguage('./assets/lib/language.json', 'kk');
				break;
				case 'az':
				$.MultiLanguage('./assets/lib/language.json', 'az');
				break;
				case 'az-AZ':
				$.MultiLanguage('./assets/lib/language.json', 'az');
				break;
				case 'hy':
				$.MultiLanguage('./assets/lib/language.json', 'hy');
				break;
				case 'hy-AM':
				$.MultiLanguage('./assets/lib/language.json', 'hy');
				break;
				case 'el':
				$.MultiLanguage('./assets/lib/language.json', 'el');
				break;
				case 'el-GR':
				$.MultiLanguage('./assets/lib/language.json', 'el');
				break;
				default:
				$.MultiLanguage('./assets/lib/language.json', 'ru');
			}

		});