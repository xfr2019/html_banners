

function submit(url, data, success, error) {

	var asyn = true;

	if (typeof data == 'function') {
		error = success;
		success = data;
		data = null;
	}

	var request = new XMLHttpRequest();
	request.open('GET', url, asyn);

	if (asyn) {
		request.onload = function() {
			if (request.status == 200) {
				try {
					data = JSON.parse(request.responseText);
				} catch($e) {
					data = request.responseText;
				}
				if (typeof success == 'function') success(data);
			} else {
				if (typeof error == 'function') error(request.status);
			}
		};
		request.onerror = function(err) {
			if (typeof error == 'function') error(err);
		};
	}

	if (data) {
		request.send(data);
	} else {
		request.send();
	}

	if (!asyn) {
		if (request.status == 200) {
			try {
				data = JSON.parse(request.responseText);
			} catch($e) {
				data = request.responseText;
			}
			if (typeof success == 'function') success(data);
		} else {
			if (typeof error == 'function') error();
		}
	}
}

//$("#slideshow > article:gt(0)").hide();


function load(lang, sportIds) {
	var sportId = sportIds.shift();
	if (!sportId) { 
		console.error('no events') ;
		return;
	} 


	submit('https://line02.bkfon-resource.ru/line/mobile/showEvents?lang=' + lang +'&lineType=full_line&sportId=' + sportId, function(data) {
		var events = data.events.filter(event => event.parentId === 0);

		if (!events.length) return load(lang, sportIds);
		var index = -1;  

		function render() {
			index = (index + 1) % events.length;
			var event = events[index];
			const [startDate, startTime] = event.startTime.split(' ', 2);
			const sportName = event.sportName.split('.').slice(1).join(' / ');

			//document.querySelector("[data-content=EVENT]").innerText = event.sportName;

			Array.prototype.forEach.call(document.querySelectorAll("[data-content=EVENT]"), function (element) {
				element.innerText =  sportName;
			});

			Array.prototype.forEach.call(document.querySelectorAll("[data-content=STARTTIME]"), function (element) {
				element.innerText =  startTime;
			});
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=STARTDATE]"), function (element) {
				element.innerText =  startDate;
			});
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=TEAM1]"), function (element) {
				element.innerText =  event.team1;
			});
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=TEAM2]"), function (element) {
				element.innerText =  event.team2;
			});
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=LOGO1]"), function (element) {
				element.setAttribute("src", "https://logo.fonstatz.com/logos/1/" + encodeURIComponent(event.team1) + ".png");
			});
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=LOGO2]"), function (element) {
				element.setAttribute("src","https://logo.fonstatz.com/logos/1/" + encodeURIComponent(event.team2) + ".png");
			});


			var subcategory = event.subcategories.find(subcategory => subcategory.type === 0);
			if (!subcategory) {
				return render();
			}

			var quotes = subcategory.quotes;
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=QUOTE1]"), function (element) {
				element.innerText = quotes[0].quote;
			});
			Array.prototype.forEach.call(document.querySelectorAll("[data-content=QUOTE2]"), function (element) {
				element.innerText = quotes[1].quote;
			});
			// Array.prototype.forEach.call(document.querySelectorAll("[data-content=QUOTEX]"), function (element) {
			// 	element.innerText = quotes[2].quote; 
			// });
			
		}
		// setInterval(() => {
		// 	var $first = $('#slideshow > article:first').fadeOut(1000);
		// 	var $next = $first.next();				
		// 	if ($next.has('[data-content=STARTTIME]').length) {			
		// 		render();
		// 	}

		// 	$next.fadeIn(1000);
		// 	$first.appendTo('#slideshow');
		// }, 6000);
		setInterval(render, 1000);
		render();
	});
}